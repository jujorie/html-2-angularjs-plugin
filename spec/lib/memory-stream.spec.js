'use strict';

describe('memory-stream', () => {
    const MemoryStream = require('../../lib/memory-stream');

    it('Must Write to memory', () => {
        const writer = new MemoryStream();
        writer.write('1');
        writer.write('2');
        writer.write('3');
        expect(writer.buffer.toString()).toEqual('123');
    });

    it('Must Change Buffer', () => {
        const writer = new MemoryStream();
        writer._write('test', 'utf8', () => {});
        expect(writer.buffer.toString()).toBe('test');
    });
});
