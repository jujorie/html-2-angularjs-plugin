'use strict';

describe('html-2-angularjs-plugin', () => {
    const {compile} = require('../utils/webpack-utils');
    const {readFileSync} = require('fs');
    const {resolve} = require('path');
    const Plugin = require('../../lib');

    const read = (path) => readFileSync(resolve(__dirname, path)).toString();

    describe('Plugin Process', () => {
        it('Must generate file', () => {
            return compile()
                .then(() => {
                    const expected = read('../fixtures/result_1.js');
                    const result = read('../../tmp/template.js');
                    expect(result).toEqual(expected);
                });
        });

        it('Must generate file with null options', () => {
            return compile({
                plugins: [
                    new Plugin(),
                ],
            })
                .then(() => {
                    const expected = read('../fixtures/result_1.js');
                    const result = read('../../tmp/template.js');
                    expect(result).toEqual(expected);
                });
        });

        it('Must generate file excluding a file', () => {
            return compile({
                plugins: [
                    new Plugin({
                        exclude: /file2\.html/,
                    }),
                ],
            })
                .then(() => {
                    const expected = read('../fixtures/result_2.js');
                    const result = read('../../tmp/template.js');
                    expect(result).toEqual(expected);
                });
        });
    });
});
