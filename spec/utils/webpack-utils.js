'use strict';
((module) => {
    const Plugin = require('../../lib');
    const webpack = require('webpack');
    const {resolve} = require('path');

    const DEFAULT_OPTIONS = {
        mode: 'development',
        entry: {
            main: resolve(__dirname, '../fixtures/index.js'),
        },
        output: {
            filename: '[name].js',
            path: resolve(__dirname, '../../tmp/'),
        },
        plugins: [
            new Plugin({}),
        ],
        module: {
            rules: [
                {
                    test: /\.html$/,
                    use: [{
                        loader: 'file-loader',
                        options: {
                            name: '[name].[ext]',
                        },
                    }],
                },
            ],
        },
    };

    /**
     * Compile webpack
     * @param {object} options - Weback options
     * @return {Promise}
     */
    function compile(options={}) {
        return new Promise((resolve, reject)=>{
            const config = Object.assign({}, DEFAULT_OPTIONS, options);
            webpack(config, (err, stat) => {
                if (err) {
                    reject(err);
                } else if (stat.hasErrors()) {
                    reject(stat.toString());
                } else {
                    resolve(stat);
                }
            });
        });
    }

    module.exports = {
        compile,
    };
})(module);
