# html-2-angularjs-plugin

Webpack plugin to extract html files to js

## Table of contents

* [Installation](#installation)
* [Dependencies](#dependencies)
* [Usage Example](#usage-example)
* [Options](#options)

## Installation

To install the plugin

```sh
$ npm install -D @jujorie/html-2-angularjs-plugin
```

## Dependencies

This plugin depends on 

* [html-minifier](https://www.npmjs.com/package/html-minifier) To minify the html code on the templates

## Usage Example

Like __webpack.config.js__ 

```javascript
const html2AngularjsPlugin = require('@jujorie/html-2-angularjs-plugin');

module.exports = {
    plugins: [
        new html2AngularjsPlugin({
            moduleName: 'app',
            filename: 'output.js',
            test: /\.html/,
            exclude: /index\.html/
        })
    ]
}
```

## Options

Plugin options

* __filename__ 
    Name of the generated file.
    
    _default_: 'template.js'

* __moduleName__
    Name of the angular module

    _default_ 'app'

* __test__
    Regexp to find the html files

    _default_ /\.html/

* __exclude__
    Regexp to select files to exclude

    _default_ null