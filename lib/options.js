/**
* Options for the Plugin
* @typedef Html2AngularJsPluginOptions
* @property {string} [filename='template.js']
* @property {string} [moduleName='app']
* @property {Regexp} [test=Regexp('\.html$')]
* @property {Regexp} [exclude]
*/
module.exports = {
    filename: 'template.js',
    moduleName: 'app',
    test: /\.html$/,
    exclude: null,
};

