'use strict';

const htmlMinifier = require('html-minifier');
const minify = htmlMinifier.minify;
const MemoryStream = require('./memory-stream');
const DEFAULT_OPTIONS = require('./options');
const {RawSource} = require('webpack-sources');

/**
 * Compiles all html files an add to a angular cache
 */
class Html2AngularJsWebpackPlugin {
    /**
     * Create new instance of Html2AngularJsWebpackPlugin
     * @param {Html2AngularJsPluginOptions} options - Plugin options
     */
    constructor(options) {
        /**
         * Plugin Settings
         * @type {Html2AngularJsPluginOptions}
         * @description Valid plugin settings
         */
        this.settings = Object.assign({}, DEFAULT_OPTIONS, options || {});
    }

    /**
     * Get the source content
     * @param {string} templateContent - template content
     * @return {string}
     */
    escape(templateContent) {
        return templateContent
            .replace(/\\/g, '\\\\')
            .replace(/'/g, '\\\'')
            .replace(/\r?\n/g, '\\n');
    }

    /**
     * Apply the plug in
     * @param {Object} compiler
     */
    apply(compiler) {
        compiler.hooks.emit.tapAsync('emit', (compilation, next) => {
            const moduleName = this.settings.moduleName;
            const filename = this.settings.filename;
            const out = new MemoryStream();
            out.write(`var app = angular.module('${moduleName}');`);
            out.write(`app.run(['$templateCache', function($templateCache){`);
            out.on('finish', () => {
                const content = out.buffer.toString();
                compilation.assets[filename] = new RawSource(content);
                next();
            });

            Object
                .keys(compilation.assets)
                .filter((assetKey) => {
                    return this.settings.test.test(assetKey)
                        && (!this.settings.exclude
                            || !this.settings.exclude.test(assetKey)
                        );
                })
                .forEach((assetKey) => {
                    let htmlContent = null;
                    let templateContent = null;
                    let assetSource = null;
                    const asset = compilation.assets[assetKey];
                    assetSource = asset.source();

                    /* istanbul ignore next */
                    if (!Buffer.isBuffer(assetSource)) {
                        assetSource = new Buffer(assetSource, 'utf8');
                    }
                    htmlContent = assetSource.toString();
                    templateContent = this.escape(
                        minify(htmlContent, {collapseWhitespace: true})
                    );
                    out.write(`$templateCache`);
                    out.write(`.put('${assetKey}', '${templateContent}');`);
                    console.log('*', assetKey);
                });

            out.end('}]);');
        });
    }
}

module.exports = Html2AngularJsWebpackPlugin;
