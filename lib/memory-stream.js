'use strict';

const {Writable} = require('stream');

/**
 * Create a new Memory Stream
 */
class MemoryStream extends Writable {
    /**
     * Create a new instance of
     * @param {internal.WritableOptions} opts - Buffer options
     */
    constructor(opts) {
        super(opts);
        this._innerBuffer = new Buffer('');
    }

    /**
     * Get the memory buffer
     * @return {Buffer}
     */
    get buffer() {
        return this._innerBuffer;
    }

    /**
     * Writes to stream
     * @param {Buffer|string} chunk - Chunk to be added
     * @param {string} encoding - Encoding
     * @param {Function} next - callback
     */
    _write(chunk, encoding, next) {
        const buffer = Buffer.isBuffer(chunk)
            ? chunk
            : new Buffer(chunk, encoding);
        this._innerBuffer = Buffer.concat([this._innerBuffer, buffer]);
        next();
    }
}

module.exports = MemoryStream;
